<?php 
	class Administracion{
		
	    private $loader;
	    private $twig;
	    private $model;

		private $response = array(
			"status" => "",
			"body" => "",
			"data" => ""
		);

	    public function __construct(){	    	
			require_once 'model/Asistencia.php';
			$this->model = new Asistencia();
			
	    	$this->loader = new \Twig\Loader\FilesystemLoader('views/');
	    	$this->twig = new \Twig\Environment($this->loader, [ 
	    	 	/*'cache' => 'cache/',
	    		'debug' => true*/
	    	]);
	    }

	    public function index(){
	    	 echo $this->twig->render('reg_diario/index.twig', ['usuario' => 'Mardxc']);	    	
	    }

	    public function create(){
	    	 echo $this->twig->render('reg_diario/create.twig');	    	
	    }

	    public function guardar_registro_diario($data){
	    	 $this->response["status"] 		= "ok";
	    	 $this->response["body"] 		= "Registros encontrados";
	    	 $this->response["data"] 		= $this->model->guardar_registro_diario($data);
	    	 //$this->response 		= $this->model->listar_ruta();
	    	 echo json_encode($this->response);
	    }
	}

 ?>