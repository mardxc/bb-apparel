<?php 
	class Proveedor{		
	    private $loader;
	    private $twig;
	    private $model;

		private $response = array(
			"status" 	=> "",
			"body" 		=> "",
			"data" 		=> array(
				"todos" 	=>	"",
				"proveedor" =>"",
				"ruta" 		=> ""
			)
		);

		//private $response;

	    public function __construct(){	    	
			require_once 'model/Proveedor.php';
			$this->model = new Proveedores();

	    	$this->loader = new \Twig\Loader\FilesystemLoader('views/');
	    	$this->twig = new \Twig\Environment($this->loader, [
	    	 	/*'cache' => 'cache/',
	    		'debug' => true*/
	    	]);
	    }

	    public function index(){	    	
	    	 echo $this->twig->render('proveedor/index.twig', ['usuario' => 'Mardxc']);	    	
	    }

	    public function listar_proveedor_ruta(){
	    	$this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros encontrados";
	    	$this->response["data"] 		= $this->model->listar_proveedor_ruta();
	    	//$this->response 		= $this->model->listar_proveedor_ruta();
	    	echo json_encode($this->response);
	    }

	    public function listar_proveedor_ruta_reg_diario($data){
			require_once 'model/Ruta.php';
			require_once 'model/Asistencia.php';
			$this->model_ruta = new Rutas();
			$this->model_asistencia = new Asistencia();

	    	$this->response["status"] 		= "ok";
			$this->response["body"] 		= "Registros encontrados";
			$proveedor_ruta = $this->model->listar_proveedor_ruta_reg_diario($data);
			$proveedor 		= $this->model->listar_proveedor_hoy($data);
			$ruta 			= $this->model_ruta->listar_ruta_hoy($data);
			$ultima_fecha	= $this->model->validar_rutas_fecha_actual($data);
			/*if(empty($proveedor_ruta)){
				$proveedor_ruta = $this->model->listar_proveedor_ruta();
			}*/
			if(empty($ultima_fecha)){
				$proveedor_ruta = $this->model->listar_proveedor_ruta();
			}

			if(empty($proveedor)){
				$proveedor = $this->model->listar_proveedor();
			}

			if(empty($ruta)){
				$ruta = $this->model_ruta->listar_ruta();
			}
			$this->response["data"]["todos"] 		= $proveedor_ruta;
			$this->response["data"]["proveedor"] 	= $proveedor;
			$this->response["data"]["ruta"] 		= $ruta;
	    	//$this->response 		= $this->model->listar_proveedor_ruta_reg_diario();
	    	echo json_encode($this->response);
	    }

	    public function listar_proveedor(){
	    	$this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros encontrados";
	    	$this->response["data"] 		= $this->model->listar_proveedor();
	    	//$this->response 		= $this->model->listar_proveedor();
	    	echo json_encode($this->response);
	    }

	}

 ?>