/* AQUI VA CUALQUIER INSTRUCCION ESCRITA VIA JQUERY */

/*
$(document).ready(function() {
    var tb_reporte_rutas = $('#tb_reporte_rutas').DataTable({
    	bFilter: 		false,
    	info:     		false,
    	paging:         false,
    	scrollCollapse: true,
    	scrollY:        "160px",
    	fixedColumns:   {
    	    leftColumns: 1
    	}
    });
} );*/

var app_reporte_semanal = new Vue({
  el: "#reporte_semanal",
  data: {
    url: url_server,
    ruta_dia: [],
    info: [],
    num_semana: ""
  },
  mounted: function() {
    this.$http
      .post(this.url + "?route=reporte&controller=get_num_semana_actual")
      .then(
        response => {
          //JAVI
          //PARCHE PARA OBTENER LA FECHA ACTUAL Y CON BASE A ELLA CONOCER LA SEMANA EN QUE SE ENCUENTRA
          // Y ASI MOSTRAR EL REPORTE (POR DEFAULT) DE LA SEMANA ACTUAL
          var n = response.data;
          c = n.split("");
          var n_sem = "";
          for (var i = 0; i < c.length; i++) {
            if (
              c[i] == "0" ||
              c[i] == "1" ||
              c[i] == "2" ||
              c[i] == "3" ||
              c[i] == "4" ||
              c[i] == "5" ||
              c[i] == "6" ||
              c[i] == "7" ||
              c[i] == "8" ||
              c[i] == "9"
            ) {
              n_sem = n_sem + c[i];
            }
          }
          num = parseInt(n_sem);
          //this.listar_reporte_semanal(num);
          $("#num_semana").html("");
          for (var i = 1; i < 53; i++) {
            if (num == i) {
              $("#num_semana").append(
                "<option value=" + num + " selected>Semana " + num + "</option>"
              );
              this.num_semana = num;
            } else {
              $("#num_semana").append(
                "<option value=" + i + ">Semana " + i + "</option>"
              );
            }
          }

          this.listar_reporte_semanal_fecha(this.num_semana);
          this.agregarClasesEstilosDT();
        },
        function() {
          console.log("Error al traer la semana actual");
        }
      );
  },
  created: function() {},
  methods: {
    // COSAS MAL HECHAS QUE JALAN
    listar_reporte_semanal_fecha: function(num_semana) {
      $("#tb_reporte_ruta").DataTable({
        destroy: true,
        ajax: {
          method: "POST",
          data: { num_semana: app_reporte_semanal.num_semana },
          url: url_server + "?route=reporte&controller=select_reporte_semanal"
        },
        order: [[0, "desc"]],
        aoColumns: [
          { sWidth: "auto", data: "id_ruta" },
          { sWidth: "auto", data: "ruta" },
          //LUNES
          { sWidth: "auto", data: "lunes_personal_recibido" },
          { sWidth: "auto", data: "lunes_hora_llegada" },
          { sWidth: "auto", data: "lunes_hora_esperada" },
          //MARTES
          { sWidth: "auto", data: "martes_personal_recibido" },
          { sWidth: "auto", data: "martes_hora_llegada" },
          { sWidth: "auto", data: "martes_hora_esperada" },
          //MIERCOLES
          { sWidth: "auto", data: "miercoles_personal_recibido" },
          { sWidth: "auto", data: "miercoles_hora_llegada" },
          { sWidth: "auto", data: "miercoles_hora_esperada" },
          //JUEVES
          { sWidth: "auto", data: "jueves_personal_recibido" },
          { sWidth: "auto", data: "jueves_hora_llegada" },
          { sWidth: "auto", data: "jueves_hora_esperada" },
          //VIERNES
          { sWidth: "auto", data: "viernes_personal_recibido" },
          { sWidth: "auto", data: "viernes_hora_llegada" },
          { sWidth: "auto", data: "viernes_hora_esperada" },
          //PROVEEDOR
          { sWidth: "auto", data: "proveedor" }
        ],
        columnDefs: [
          { width: "10%", targets: [0] },
          { title: "N&#176;", className: "center", targets: [0] }
          /*{ "title": "S", className: "center", "targets": [3]}*/
        ],
        processing: true,
        dom: "lBfrtip",
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por pagina",
            "zeroRecords": "Registros no encomtrados",
            "info": "Mostrando _PAGE_ paginas de _PAGES_",
            "infoEmpty": "Registros no disponibles",
            "infoFiltered": "(filtered from _MAX_ total records)"
            //"url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
        },
        buttons: [
          {
            extend: "collection",
            text: " Obtener Reporte",
            className: "btn btn-info fa fa-download",
            buttons: ["excelHtml5", "pdf"]
          }
        ],
        scrollY: "8%",
        sScrollX: "100%",
        sScrollXInner: "110%",
        bScrollCollapse: true,
        scrollCollapse: true
      });
      this.agregarClasesEstilosDT();
    },
    // COSAS MAL HECHAS QUE JALAN
    listar_reporte_semanal: function(num_semana) {
      //let co = app_reporte_semanal;
      this.$http
        .post(this.url + "?route=reporte&controller=select_reporte_semanal", {
          num_semana: num_semana
        })
        .then(
          response => {
            mostrar_tabla_reporte(response.data.data);
            //this.ruta_dia = response.data.data;
          },
          function() {
            console.log("Error al listar la informacion");
          }
        );
    },
    onChange(event) {
      //this.listar_reporte_semanal(this.num_semana);
      this.listar_reporte_semanal_fecha(this.num_semana);
      
    },
    agregarClasesEstilosDT: function(){
      $("#tb_reporte_ruta_length").addClass("col-md-4");
      $(".dt-buttons").addClass("col-md-4");
      $("#tb_reporte_ruta_filter").addClass("col-md-4");
      
    }
  }
});
