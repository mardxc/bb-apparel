/* AQUI VA CUALQUIER INSTRUCCION ESCRITA VIA JQUERY */
const co = app_create_reg_diario;
/*
$( window ).on( "load", function() {
	var tb_create_proveedores = $('#tb_create_proveedores').DataTable({
    	bFilter: 		false,
    	info:     		false,
    	paging:         false,
    	scrollCollapse: true,
    	scrollY:        "160px",
    	fixedColumns:   {
    	    leftColumns: 1
    	}
    });
});*/
/*
$(document).ready(function() {
    var tb_create_proveedores = $('#tb_create_proveedores').DataTable({
    	bFilter: 		false,
    	info:     		false,
    	paging:         false,
    	scrollCollapse: true,
    	scrollY:        "160px",
    	fixedColumns:   {
    	    leftColumns: 1
    	}
    });
} );*/

function guardar_registro_diario() {
  let data = {
    cantidad_ordinaria: co.registro_diario.cantidad_ordinaria,
    fecha: co.registro_diario.fecha_registro,
    descripcion: co.registro_diario.descripcion,
    personal_recibido: co.registro_diario.personal_recibido,
    id_ruta: co.registro_diario.id_ruta,
    hora_llegada: co.registro_diario.hora_llegada,
    hora_esperada: co.registro_diario.hora_esperada
  };

  $.ajax({
    url: url_server + "?route=reg_diario&controller=guardar_registro_diario",
    type: "POST",
    dataType: "JSON",
    data: { data: data },
    beforeSend: function() {},
    success: function(response) {
      app_create_reg_diario.listar_proveedor_ruta_reg_diario();

      co.registro_diario.cantidad_ordinaria = "";
      co.registro_diario.descripcion = "";
      co.registro_diario.personal_recibido = "";
      co.registro_diario.id_ruta = null;
      co.registro_diario.hora_llegada = "";
      co.registro_diario.hora_esperada = "";
    }
  });
}
