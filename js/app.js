let loc = window.location;
let pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf("/") + 1);
var url_server = loc.href.substring(
  0,
  loc.href.length -
    ((loc.pathname + loc.search + loc.hash).length - pathName.length)
);

var app_create_reg_diario = new Vue({
  el: "#app_create_reg_diario",
  data: {
    url: url_server, // VARIABLE PARA GUARDAR LA RUTA DE SERVIDOR
    proveedores: [], // ARREGLO PARA GUARDAR LOS PROVEEDORES QUE VENGAN DESDE EL BACK
    rutas: [],
    proveedores_rutas: [],
    registro_diario: {
      id_asistencia: null,
      //cantidad_extra: '',
      fecha_registro: "", //reg_fecha
      id_ruta: null, //reg_ruta
      cantidad_ordinaria: "", //reg_capacidad
      personal_recibido: "", //reg_entradas
      hora_llegada: "", //reg_hora_llegada
      descripcion: "", //reg_comentarios
      id_det_asistencia: null,
      id_comentario: null,
      hora_esperada: "",
      id_proveedor: ""
    }
  },
  mounted: function() {
    // INVOCAR METODOS DESPUES DE CARGADO EL DOM
    //this.tb_create_proveedores();
  },
  created: function() {
    // INVOCAR METODOS ANTES DE CARGADO EL DOM
    this.registro_diario.fecha_registro = this.hoyFecha();
    this.listar_proveedor_ruta_reg_diario();
    this.tb_create_proveedores();
    //this.listar_proveedor(),
    //this.listar_ruta()
  },
  methods: {
    tb_create_proveedores: function() {
      $("#tb_create_proveedores").DataTable({
        bFilter: false,
        info: false,
        paging: false,
        scrollCollapse: true,
        scrollY: "160px",
        fixedColumns: {
          leftColumns: 1
        },
        language: {
          emptyTable: "",
          zeroRecords: ""
        }
      });
    },
    listar_proveedor_ruta_reg_diario: function() {
      this.$http
        .post(
          this.url +
            "?route=proveedor&controller=listar_proveedor_ruta_reg_diario",
          {
            fecha: this.registro_diario.fecha_registro
          }
        )
        .then(
          response => {
            let res = response.data.data;
            this.proveedores_rutas = res.todos;
            this.proveedores = res.proveedor;
            this.rutas = res.ruta;
          },
          response => {
            console.log("error");
          }
        );
    },
    listar_proveedor_ruta: function() {
      this.$http
        .get(this.url + "?route=proveedor&controller=listar_proveedor_ruta")
        .then(function(response) {
          //this.proveedores_rutas = response.data.data;
          let res = response.data.data;
          this.proveedores_rutas = res;
          this.proveedores = res;
          this.rutas = res;
        });
    },
    seleccionar_ruta: function(id) {
      this.$http
        .get(
          this.url + "?route=ruta&controller=listar_ruta_especifica&id=" + id
        )
        .then(function(response) {
          $("#reg_fecha").val(new Date());
          var data = response.data.data[0];
          //$("#reg_proveedor").val(data.id_proveedor);
          this.registro_diario.id_ruta = data.id_ruta;
          this.registro_diario.id_proveedor = data.id_proveedor;
          //$("#reg_ruta").val(data.id_ruta);
          //$("#reg_capacidad").val(data.capacidad);
          this.registro_diario.cantidad_ordinaria = data.capacidad;
          this.registro_diario.hora_esperada = data.hora_llegada;
        });
    },

    onChange(event) {
      this.listar_proveedor_ruta_reg_diario();
    },

    listar_proveedor: function() {
      this.$http
        .get(this.url + "?route=proveedor&controller=listar_proveedor")
        .then(function(response) {
          this.proveedores = response.data.data;
        });
    },
    listar_ruta: function() {
      this.$http
        .get(this.url + "?route=ruta&controller=listar_ruta")
        .then(function(response) {
          this.rutas = response.data.data;
        });
    },
    hoyFecha: function() {
      var hoy = new Date();
      var dd = hoy.getDate();
      var mm = hoy.getMonth() + 1;
      var yyyy = hoy.getFullYear();

      dd = this.addZero(dd);
      mm = this.addZero(mm);
      var fecha = "" + yyyy + "-" + mm + "-" + dd + "";

      //$("#reg_fecha").val(fecha);
      return fecha;
    },
    addZero: function(i) {
      if (i < 10) {
        i = "0" + i;
      }
      return i;
    },
    guardar_registro_diario: e => {
      e.preventDefault();
      let co = app_create_reg_diario;
      let data = {
        cantidad_ordinaria: co.registro_diario.cantidad_ordinaria,
        fecha: co.registro_diario.fecha_registro,
        descripcion: co.registro_diario.descripcion,
        personal_recibido: co.registro_diario.personal_recibido,
        id_ruta: co.registro_diario.id_ruta,
        hora_llegada: co.registro_diario.hora_llegada,
        hora_esperada: co.registro_diario.hora_esperada
      };
      co.$http
        .post(
          co.url + "?route=reg_diario&controller=guardar_registro_diario",
          data,
          { headers: { "content-type": "application/json" } }
        )
        .then(
          function(response) {
            console.log(response);
            console.log("si llega");

            //response => {
            //console.log(response);
          },
          response => {
            console.log("error");
          }
        );
    }
  }
});
